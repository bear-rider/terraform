terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.8"
}

resource "aws_iam_user" "admin_user" {
  name = "admin"
}

resource "aws_iam_policy" "custom_policy" {
  name = "a_custom_policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "custompolicyexample",
            "Effect": "Allow",
            "Action": [
                "glacier:ListProvisionedCapacity"
            ],
            "Resource": "*"
        }
    ]
}
  EOF
}

resource "aws_iam_policy_attachment" "policyBind" {
  name       = "attachment"
  users      = [aws_iam_user.admin_user.name]
  policy_arn = aws_iam_policy.custom_policy.arn
}

output "iam_policy_arm" {
  value = aws_iam_policy.custom_policy.arn
}
