resource "aws_iam_user" "admin_user" {
  name = "admin"
}

module "ec2module" {
  source        = "./ec2"
  instance_name = "ec2 module"
}
