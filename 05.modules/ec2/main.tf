resource "aws_instance" "module_ec2" {
  ami           = data.aws_ami.amazon-linux-2-ami.id
  instance_type = "t2.micro"

  tags = {
    Name = "var.instance_name"
  }
}

data "aws_ami" "amazon-linux-2-ami" {
  most_recent = true
  owners      = ["amazon"] # AWS

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}
