variable "instance_name" {
  description = "the instance name passed from module declaration"
  type        = string
  default     = "something-random"
}
