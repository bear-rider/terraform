
terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.8"
}

module "instances" {
  source  = "./ec2"
  servers = ["uat", "dev", "prod"]
}

output "public_ips" {
  value = module.instances.public_ip
}