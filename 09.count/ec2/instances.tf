
variable "servers" {
  type = "list"
}

resource "aws_instance" "ec2" {
  ami                         = data.aws_ami.amazon-linux-2-ami.id
  instance_type               = "t2.micro"
  count                       = length(var.servers)
  associate_public_ip_address = true

  tags = {
    Name = var.servers[count.index]
  }
}

output "public_ip" {
  value = [aws_instance.ec2.*.public_ip]
}


data "aws_ami" "amazon-linux-2-ami" {
  most_recent = true
  owners      = ["amazon"] # AWS

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}
