
terraform {
  backend "s3" {
    bucket     = "no-variables-allowed-here"
    key        = "terraform/tfstate.tfstate"
    access_key = "aws-access-key"
    secret_key = "aws-secret-key"
    region     = "bucket-aws-region"
  }
}
