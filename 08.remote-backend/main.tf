terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.8"
}

resource "aws_s3_bucket" "tfbackend" {
  bucket = "deletablebucket-${uuid()}"

  versioning {
    enabled = true
  }

  force_destroy = true
}
