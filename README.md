# notes

## resources

- [TJ Addams course on udemy](https://github.com/addamstj/terraform-udemy)
- [terraform aws provider docs](https://www.terraform.io/docs/providers/aws/index.html)
- [config lang docs](https://www.terraform.io/docs/configuration/index.html)
- [modules registry](https://registry.terraform.io/search?provider=hashicorp%2Faws&q=aws)

## commands

- `terraform init`
- `terraform plan`
- `terraform validate`
- `terraform fmt -recursive`
- `terraform apply -auto-approve`
- `terraform destroy -auto-approve`

### other notes

- add dependencies : `depends_on = [aws_instance.db]`
- `count = 3`
- [tfswitch](https://warrensbox.github.io/terraform-switcher/)`brew install warrensbox/tap/tfswitch`
- [dynamic blocks and splat expressions etc](https://www.terraform.io/docs/configuration/expressions.html)
