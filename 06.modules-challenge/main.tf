module "db_server" {
  source = "./db"
}

module "web_server" {
  source = "./web"
}
