resource "aws_instance" "web" {
  ami             = data.aws_ami.amazon-linux-2-ami.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web_traffic.name]
  user_data       = file("./web/userdata.sh")

  tags = {
    Name = "Web"
  }
}

data "aws_ami" "amazon-linux-2-ami" {
  most_recent = true
  owners      = ["amazon"] # AWS

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}