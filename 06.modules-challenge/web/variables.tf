
variable "port_numbers" {
  type    = list(number)
  default = [80, 443]
}
