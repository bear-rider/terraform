terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.8"
}

resource "aws_vpc" "just_vpc" {
  cidr_block = "10.0.0.0/18"
  tags = {
    Name = var.vpcname
  }
}
