
variable "port_numbers" {
  type    = list(number)
  default = [80, 443]
}

variable "region" {
  description = "aws region"
  type        = string
  default     = "us-east-1"
}
variable "container_port" {
  description = "container port"
  type        = string
  default     = "3000"
}

variable "server_port" {
  description = "server port"
  type        = string
  default     = "80"
}


