#!/bin/bash
yum update -y
yum install -y docker
service docker start
docker pull bkimminich/juice-shop
docker run -d -p ${var.server_port}:${var.container_port} bkimminich/juice-shop