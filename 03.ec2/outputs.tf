output "public_ip" {
  value = aws_eip.elasticip.public_ip
}


output "ami_id" {
  value = data.aws_ami.amazon-linux-2-ami.id
}

output "check_list" {
  value = [for port in var.port_numbers : "port ${port} is open"]
}

output "expected" {
  value = aws_security_group.web_traffic[*].ingress
}
