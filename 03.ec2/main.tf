terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region  = var.region
  version = "~> 2.8"
}

resource "aws_instance" "an_ec2" {
  ami             = data.aws_ami.amazon-linux-2-ami.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web_traffic.name]
  tags = {
    Name = "terraform-101"
  }
}

resource "aws_eip" "elasticip" {
  instance = aws_instance.an_ec2.id
}

resource "aws_security_group" "web_traffic" {
  name = "allow https"

  dynamic ingress {
    iterator = port
    for_each = var.port_numbers
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic ingress {
    iterator = port
    for_each = var.port_numbers
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

data "aws_ami" "amazon-linux-2-ami" {
  most_recent = true
  owners      = ["amazon"] # AWS

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}
