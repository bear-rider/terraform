
variable "port_numbers" {
  type    = list(number)
  default = [80, 443]
}

variable "region" {
  description = "aws region"
  type        = string
  default     = "us-east-1"
}
