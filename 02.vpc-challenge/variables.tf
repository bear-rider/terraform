variable "vpcname" {
  description = "name of vpc"
  type        = string
  default     = "TerraformVPC"
}

variable "region" {
  description = "aws region"
  type        = string
  default     = "us-east-1"
}
