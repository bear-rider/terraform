terraform {
  required_version = ">= 0.12, < 0.13"
}

resource "aws_vpc" "my_vpc" {
  cidr_block = "192.168.0.0/24"
  tags = {
    Name = var.vpcname
  }
}
